#include "../include/game.hpp"
#include "../include/boat.hpp"
#include "../include/colors.hpp"

#include <iostream>
Game::Game()
{
    // ...
}

void Game::menu()
{
    char opc;
    cout << "| BATALHA NAVAL |" << endl
         << "| Deseja jogar? |" << endl
         << "|s(sim) | n(não)|" << endl
         << "|------------->";

    cin >> opc;
    if (opc == 's')
    {
        jogoDupla();
    }
    else
    {
        cout << "volte sempre!" << endl;
        EXIT_SUCCESS;
    }
}

void Game::atualizarPontuacao(int a, int *score)
{
    if (a > -1)
    {
        *score += 1;
    }
    cout << *score << endl;
}

int Game::bombardear(Board *board, int *numTiros)
{
    bool entrada = false;
    bool desistir = false;
    int linha = -1;
    int coluna = -1;
    string bomta;
    while (entrada == false)
    {

        cout << FRED("Digite a posição para bombardear: ");
        cin >> bomta;
        if (bomta == "desisto")
        {
            desistir = true;
            break;
        }
        coluna = bomta[0] - 'A';
        if (bomta.length() == 3)
        {
            linha = ((bomta[1] - '0') * 10) + (bomta[2] - '0') - 1;
        }
        else if (bomta.length() == 2)
        {
            linha = bomta[1] - '0' - 1;
        }
        if (linha >= 0 && linha <= 9 && coluna >= 0 && coluna <= 9)
        {
            entrada = true;
        }
        if (board->getCampoEscondido(linha, coluna) == -1)
        {
            entrada = false;
            cout << "Já foi dado tiro aqui;" << endl;
        }
    }
    *numTiros -= 1;
    if (numTiros == 0)
    {
        cout << "Acabou suas bombasd" << endl;
        return -10;
    }
    if (desistir == true)
    {
        return -9;
    }
    board->setEscondido(linha, coluna);
    int a = board->getCampoInt(linha, coluna);
    return a;
}

void Game::vencedor(int pontuacao, int pontuacao2) {
    if(pontuacao == pontuacao2) {
        cout << "empate!" << endl;
    }
    if(pontuacao > pontuacao2) {
        cout << "jogador 1 ganhou!" << endl;
    }
    if(pontuacao < pontuacao2) {
        cout << "hogador 2 ganhou!" << endl;
    }
    
}

void Game::jogoDupla()
{
    int opc;
    vetorBarcos(V1);
    vetorBarcos(V2);
    Board board1;
    Board board2;
    cout << "ok" << endl;

    int numTiros = 50;
    int numTiros2 = 50;
    int a = -2;
    int a2 = -2;
    int score = 0;
    int score2 = 0;

    do
    {
        system("clear");
        cout << BOLD(FCYN("Jogador 1 - Pontuação: ")); 
        atualizarPontuacao(a, &score);
        if (score >= 28)
        {
            break;
        }
        localBomba(a, V1);
        a = -2;
        board1.imprimirCampoEscondido();

        cout << BOLD(FBLU("Jogador 2 - Pontuação: "));
        atualizarPontuacao(a2, &score2);
        if (score2 >= 28)
        {
            break;
        }
        localBomba(a2, V2);
        a2 = -2;
        board2.imprimirCampoEscondido();

        cout << UNDL(BOLD(FCYN("Jogador 1: Atire:"))) << endl; 
        a = bombardear(&board2, &numTiros);
        if (a == -10 || a == -9)
        {
            break;
        }
        system("clear");
        cout << BOLD(FCYN("Jogador 1 - Pontuação: "));
        atualizarPontuacao(a, &score);
        if (score >= 28)
        {
            break;
        }
        localBomba(a, V1);
        a = -2;
        board1.imprimirCampoEscondido();


        cout << BOLD(FBLU("Jogador 2 - Pontuação: "));
        atualizarPontuacao(a2, &score2);
        if (score >= 28)
        {
            break;
        }
        localBomba(a2, V2);
        a = -2;
        board2.imprimirCampoEscondido();
        cout << UNDL(BOLD(FBLU("Jogador 2: Atire:"))) << endl; 
        a2 = bombardear(&board1, &numTiros2);
        if(a2 == -10 || a2 == -9) {
            break;
        }

    } while (1);
    system("clear");
    cout << "Campo do jogador 1:" << endl;
    board1.imprimirCampo();

    cout << "Campo do jogador 2:" << endl;
    board2.imprimirCampo();

    if(a==-9) {
        cout << "Jogador 1 desistiu, jogador 2 ganhopu" << endl;
    } else if(a2 == -9) {
        cout << "jhogador dois desistriu, jogadro 1 ganhou:!" << endl;
    } else {
        vencedor(score, score2);
    }
}

void Game::vetorBarcos(Boat V[])
{
    for (int i = 0; i < 3; i++)
    {
        V[i].setBarcos("Fragata", 2);
        V[i + 3].setBarcos("Balizador", 2);
    }
    for (int i = 6; i < 8; i++)
    {
        V[i].setBarcos("Contratorperdo", 3);
        V[i + 2].setBarcos("Cruzador", 3);
    }
    V[10].setBarcos("Porta-Aviões", 4);
}

void Game::localBomba(int a, Boat V[11])
{
    if (a == -2)
    {
        return;
    }
    if (a != 1)
    {
        V[a].barcoAtingido();
    }
}