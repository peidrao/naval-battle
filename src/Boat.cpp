#include "../include/boat.hpp"

Boat::Boat()
{
    this->nome_ = "default";
    this->tamanho_ = 0;
    this->vida_ = 0;
}

Boat::Boat(string nome, int tamanho)
{
    this->nome_ = nome;
    this->vida_ = tamanho;
    this->tamanho_ = tamanho;
}

void Boat::getBarcos()
{
    cout << "Nome: " << this->nome_ << " | "
         << "Tamanho: " << this->tamanho_ << " | "
         << "Vida: " << this->vida_ << endl;
}

void Boat::barcoAtingido()
{
    if (this->vida_ > 0)
    {
        this->vida_--;
        cout << "torei-me" << endl;
    }
    if (this->vida_ == 0)
        cout << "destruido" << endl;
}

void Boat::setBarcos(string nome, int tamanho)
{
    nome_ = nome;
    tamanho_ = tamanho;
    vida_ = tamanho;
}