#include "../include/board.hpp"
#include <iostream>
#include <experimental/random>
#include <stdlib.h>
#include "../include/colors.hpp"

using namespace std;

void Board::iniciarCampo(char campo[][10])
{
    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            campo[i][j] = '~';
        }
    }
}

void Board::iniciarCampoInt(int campo[][10])
{
    for (int i = 0; i <= 10; i++)
    {
        for (int j = 0; j <= 10; j++)
        {
            campo[i][j] = -1;
        }
    }
}

void Board::imprimirCampoEscondido()
{
    cout << FYEL("     A   B   C   D   E   F   G   H   I   J ") << endl;
    cout << FYEL("--------------------------------------------") << endl;
    for (int i = 0; i < 10; i++)
    {
        if (i < 9)
        {
            cout << ' ' << i + 1 << FYEL(" | ");
        }
        else
        {
            cout << (i+1) << " | ";
        }
        for (int j = 0; j <10; j++)
        {
            cout << campoEscondido2[i][j] << FYEL(" | ");
            if (j == 9)
            {
                cout << endl;
            }
        }
    }
    cout << FYEL("--------------------------------------------") << endl;
}

void Board::setEscondido(int lin, int col) {
    if(campoCodigos[lin][col] != -1){
        campoEscondido2[lin][col] = '0';
    } else {
        campoEscondido2[lin][col] = 'X';
    }
}

int Board::getCampoInt(int lin, int col) {
    if(campoCodigos[lin][col] != -1) {
        return(campoCodigos[lin][col]);
    }
    return -1;
}

int Board::getCampoEscondido(int lin, int col) {
    if(campoEscondido2[lin][col] != '~') {
        return -1;
    }
    return 1;
}

void Board::imprimirCampo()
{
    cout << "   A B C D E F G H I J " << endl;
    for (int i = 0; i <10; i++)
    {
        if (i < 9)
        {
            cout << ' ' << i + 1 << ' ';
        }
        else
        {
            cout << i + 1 << ' ';
        }
        for (int j = 0; j < 10; j++)
        {
            cout << campoEscondido[i][j] << ' ';
            if (j == 9)
            {
                cout << endl;
            }
        }
    }
}

void Board::imprimirCampoInt()
{
     cout << " A B C D E F G H I J " << endl;
    for (int i = 0; i <= 10; i++)
    {
        if (i < 10)
        {
            cout << ' ' << i + 1 << ' ';
        }
        else
        {
            cout << i << ' ';
        }
        for (int j = 0; j <= 10; j++)
        {
            cout << campoCodigos[i][j] << ' ';
            if (j == 10)
            {
                cout << endl;
            }
        }
    }
}

void Board::posicionarBarco(int linha, int coluna, char barco, char orientacao, int codigoBarco)
{
    int tamBarco;
    if (barco == 'F' || barco == 'B')
    {
        tamBarco = 2;
    }
    if (barco == 'C' || barco == '+')
    {
        tamBarco = 3;
    }
    if (barco == 'P')
    {
        tamBarco = 4;
    }

    int lin = linha, col = coluna, disponivel = 0;
    if (orientacao == 'V')
    {
        while (disponivel != tamBarco)
        {
            disponivel = 0;
            posicao(&lin, &col, tamBarco, 'V');
            for (int i = 0; i < tamBarco; i++)
            {
                if (campoEscondido[lin + i][col] == '~')
                {
                    disponivel++;
                }
            }
        }
        for (int i = 0; i < tamBarco; i++)
        {
            campoEscondido[lin + i][col] = barco;
            campoCodigos[lin + i][col] = codigoBarco;
        }
    }
    if (orientacao == 'H')
    {
        while (disponivel != tamBarco)
        {
            disponivel = 0;
            posicao(&lin, &col, tamBarco, 'H');
            for (int i = 0; i < tamBarco; i++)
            {
                if (campoEscondido[lin][col + i] == '~')
                {
                    disponivel++;
                }
            }
        }
        for (int i = 0; i < tamBarco; i++)
        {
            campoEscondido[lin][col + i] = barco;
            campoCodigos[lin][col + i] = codigoBarco;
        }
    }
    if (orientacao == 'D')
    {
        int i = 0, j = 0;
        while (disponivel != tamBarco)
        {
            disponivel = 0;
            i = 0;
            j = 0;
            posicao(&lin, &col, tamBarco, 'D');
            for (int i = 0; i < tamBarco; i++)
                while (i < tamBarco && j < tamBarco)
                {
                    if (campoEscondido[lin + i][col + j] == '~')
                    {
                        disponivel++;
                    }
                    i++;
                    j++;
                }
        }
        i = 0, j = 0;
        while (i < tamBarco && j < tamBarco)
        {
            campoEscondido[lin + i][col + j] = barco;
            campoCodigos[lin + i][col + j] = codigoBarco;
            i++;
            j++;
        }
    }
}
void Board::posicao(int *linha, int *coluna, int tamBarco, char orientacao)
{
    bool posicaoo = false;
    if (orientacao == 'V')
    {
        *coluna = experimental::randint(0, 9);
        while (posicaoo == false)
        {
            *linha = experimental::randint(0, 10);
            if (10 - *linha >= tamBarco)
            {
                posicaoo = true;
            }
        }
    }
    if (orientacao == 'H')
    {
        *linha = experimental::randint(0, 9);
        while (posicaoo == false)
        {
            *coluna = experimental::randint(0, 9);
            if (10 - *coluna >= tamBarco)
            {
                posicaoo = true;
            }
        }
    }
}

void Board::barcosRandom(char campo[][10], int campo2[][10])
{
    int post;
    char V[2] = {'H', 'V'};
    for (int i = 0; i < 3; i++)
    {
        post = experimental::randint(0, 2);
        posicionarBarco(-1, -1, 'F', V[post], i);
        post = experimental::randint(0, 2);
        posicionarBarco(-1, -1, 'B', V[post], i + 3);
    }
    for (int i = 6; i < 8; i++)
    {
        post = experimental::randint(0, 2);
        posicionarBarco(-1, -1, 'C', V[post], i);
        post = experimental::randint(0, 2);
        posicionarBarco(-1, -1, '+', V[post], i + 2);
    }
    post = experimental::randint(0, 2);
    posicionarBarco(-1, -1, 'P', V[post], 10);
}
