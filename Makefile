all: 
	clear
	g++ ./src/Board.cpp ./src/Game.cpp  ./src/Player.cpp ./src/Boat.cpp ./src/Main.cpp -std=c++11 -o ./bin/navalBattle.o
clean:
	rm -rf ./bin/navalBattle.o 
run:
	./bin/navalBattle.o
	
