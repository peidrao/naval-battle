#ifndef GAME_HEADER
#define GAME_HEADER

#include "../include/board.hpp"
#include "../include/boat.hpp"

#include <iostream>
using namespace std;

class Game {
    public:
    Game();
        void menu();
        void    jogoDupla() ;
        int     bombardear(Board *board, int*);
        void    vetorBarcos(Boat V[]);
        void    localBomba(int, Boat V[11]);
        void    atualizarPontuacao(int, int*);
        void    vencedor(int, int);
    private:
    Boat V1[11], V2[11];
};

#endif
