#ifndef BOAT_HEADER
#define BOAT_HEADER

#include <iostream>
#include <string>

using namespace std;


class Boat {
    private:
        int tamanho_;
        int vida_;
        string nome_;
    public:
        Boat(); /* Construtor padrão */
        Boat(string name, int tam); /* Construtor personalizado */

        void getBarcos();
        void setBarcos(string nome, int tam);
        void barcoAtingido();
};

#endif 
