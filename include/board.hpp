#ifndef BOARD_HEADER
#define BOARD_HEADER

class Board {
    private:
    char campoEscondido[10][10];
    int campoCodigos[10][10];
    char campoEscondido2[10][10];
    public:
    Board() {
        iniciarCampo(campoEscondido);
        iniciarCampoInt(campoCodigos);
        iniciarCampo(campoEscondido2);
        barcosRandom(campoEscondido, campoCodigos);
    };
    void setEscondido(int linha, int coluna);
    void iniciarCampo(char Campo[][10]);
    void iniciarCampoInt(int Campo[][10]);
    void posicionarBarco(int, int, char, char, int );
    void barcosRandom(char campo[][10], int campo2[][10]);/*Distribuir barcos*/
    void imprimirCampoEscondido();
    void imprimirCampo();
    void imprimirCampoInt();
    void posicao(int*, int*, int, char);  
    int getCampoEscondido(int, int);  
    int getCampoInt(int, int);

};

#endif 
